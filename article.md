---
title: "Chybí budoucí hejtmanky, nastupují protisystémové strany: prohlédněte si krajské kandidátky v grafech"
perex: "Sociální demokraté postaví do voleb překvapivě mnoho žen, ale žádnou lídryni. ANO bude mít kandidátky téměř stoprocentně z politických nováčků a křesťanští demokraté jsou nejvzdělanější. Co o stranách říkají kandidátky?"
description: "Sociální demokraté postaví do voleb překvapivě mnoho žen, ale žádnou lídryni. ANO bude mít kandidátky téměř stoprocentně z politických nováčků a křesťanští demokraté jsou nejvzdělanější. Co o stranách říkají kandidátky?"
authors: ["Jan Boček", "Jan Cibulka", "Jan Pospíšil"]
published: "29. srpna 2016"
socialimg: https://interaktivni.rozhlas.cz/krajske-kandidatky/media/socimg.png
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "krajske-kandidatky"
libraries: [jquery, highcharts]

---

Krajské volby před čtyřmi lety rezonovaly otázkou, kolik krajů ovládne *ČSSD* (ze třinácti možných zvítězila v devíti). Volby do krajů letošního 7. října mají jiný náboj: plné kandidátky postaví hnutí *ANO 2011*, které bylo před čtyřmi lety v plenkách. Volební preference očekávají mezi oběma stranami boj tělo na tělo, podle [červencového průzkumu agentury MEDIAN](http://www.median.eu/cs/volebni-model-median-cervenec-2016/) vede celorepublikově o čtyři procentní body ANO.

Většina ze 735 kandidátů strany před čtyřmi lety nekandidovala. Oproti komunálním volbám 2014, kde ANO postavilo řadu kandidátek na přeběhlících z jiných stran, se tentokrát podařilo Babišovým lídrům poskládat kandidátky převážně ze členů strany. Jednou z vysoce postavených výjimek je náměstkyně ministra vnitra Jana Vildumetzová, která v minulých krajských volbách kandidovala za ODS. Letos ji ANO postaví jako lídryni v Karlovarském kraji.

„Co se týká minulosti našich kandidátů, aktivní účast v minulých krajských volbách by nevadila,“ připouští jednička ANO v Pardubickém kraji Jan Řehounek. „Máte ale pravdu v tom, že na kandidátce jsou hlavně lidé, pro které budou letošní krajské volby premiérové. To platí i o mně,“ dodává.

Prohlédněte si, které strany letos kandidují ve vašem kraji (s výjimkou Prahy, té se krajské volby netýkají). Tabulku můžete seřadit podle počtu koaličních stran, kandidátů s akademickým titulem, podílu politických nováčků, zaplněnosti kandidátky nebo podílu žen.

<aside class="big">
  <iframe src="https://samizdat.cz/data/volby-16-kraje-widget/www/" class="ig" width="1024" height="840" scrolling="no" frameborder="0"></iframe>
</aside>

## Souboj nesystémových stran: Ne ilegální imigraci vs. Ilegální imigraci ne

Ve volbách se představí také řada nespokojených nebo otevřeně protisystémových stran, často postavených na odporu vůči imigraci. Z nuly na stovky kandidátů posílily například strany *Úsvit – národní koalice*, *Svoboda a přímá demokracie – Tomio Okamura*, *Národní demokracie*, *SPR-RSČ Miroslava Sládka* nebo *Domov s Blokem proti islamizaci*.

„Kandidátky jsme otevřeli všem vlastenecky smýšlejícím občanům, kteří s námi sdílejí stejný pohled na svět okolo a mají odvahu kandidovat,“ osvětluje vznik kandidátek Adam Bartoš z krajně pravicové Národní demokracie. „Tento nevšední krok jsme udělali i proto, že po jarní policejní razii v sídle strany nám policie zabavila veškerá elektronická zařízení i data, takže jsme přišli o částečně rozpracované kandidátky, čímž jsme získali zpoždění,“ doplňuje Bartoš. Ústavní soud ovšem incident z konce dubna [za nezákonný nepovažuje](http://www.denik.cz/z_domova/ustavni-soud-odmitl-stiznost-narodni-demokracie-na-prohlidku-sidla-20160630.html).

<aside class="big" style="overflow:scroll">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/data/kandidatky-2016-text/www/media/kandidatky12-16.png" width="1024" style="width:100%;min-width:620px">
  </figure>
</aside>

Oproti minulým krajským volbám početně posílila také *Strana zdravého rozumu* bývalého hudebníka Petra Hanniga. Letos s několika menšími stranami postaví koalici *Ne ilegální imigraci – peníze raději pro naše lidi*. Stejná strana v minulosti kandidovala pod názvy *Suverenita* nebo *Nechceme euro – za Evropu svobodných států*.

„My máme sice tenhle název, ale jsme otevření světu,“ komentuje současný název mozek strany Petr Hannig. „Já miluju Evropu. Ty změny názvu jsou jen volební strategie. Nemáme totiž peníze,“ dodává.

V Ústeckém kraji mu navíc bude konkurovat koalice s názvem *Ilegální imigraci ne – peníze raději pro naše lidi*. Pod téměř stejným obalem se ovšem ukrývá jiný obsah – konkurenční koalice vedená jedněmi ze dvou *republikánů*. Proti sobě tak nastoupí koalice Ilegální imigraci ne a Ne ilegální imigraci.

Oproti minulým krajským volbám budou naopak chybět *Věci veřejné* a *Suverenita – Blok Jany Bobošíkové*. Výrazně méně kandidátů než před čtyřmi lety postaví *Strana práv občanů* (letos bez dodatku *Zemanovci*), *SNK ED*, *Strana svobodných občanů* nebo *Strana zelených*.

Slabší počet kandidátů ovšem v tomto případě nemusí znamenat ústup ze slávy. „Za menší počet kandidátů může pouze to, že častěji kandidujeme v koalicích,“ obhajuje nižší čísla mluvčí zelených Pavel Bartošek.

## Mezi kandidáty na hejtmana chybí ženy

Pravicové strany už tradičně na své kandidátky zařadí málo žen. Vůbec nejmenší podíl – 15 procent – jich má mezi celorepublikovými stranami Strana svobodných občanů. Její kandidáti jsou také v porovnání s jinými stranami velmi mladí, věkový průměr svobodných je těsně nad čtyřicítkou. Strana svobodných občanů je tedy zejména stranou mladých mužů.

<aside class="big">
  <iframe src="https://samizdat.cz/data/volby-2016-kandidatky/charts/zeny.html" class="ig" width="1024" height="600" scrolling="no" frameborder="0"></iframe>
</aside>

Nejcitlivější k zastoupení žen jsou mezi celorepublikovými stranami sociální demokraté s téměř 43 procenty žen. Papírově vyváženější kandidátní listiny ovšem kazí fakt, že na prvních místech – tedy mezi kandidáty na hejtmana – žádná žena není. Sociální demokraté je umístili na hůře volitelné pozice.

Strana zelených, která hlasitě prosazuje rovnost příležitostí, má na kandidátkách žen méně, jen o něco více než třetinu.

„Pravidlem je podle stanov mít v každé trojici jednu osobu opačného pohlaví,“ vysvětluje mluvčí strany Pavel Bartošek. „Nelze tedy tvrdit, že strana o rovných příležitostech jen mluví. Nebojíme se genderové kvóty uplatňovat i v praxi,“ dodává.

Ani u zelených ale v krajských volbách nenajdeme mnoho lídryní. Na první místo koaličních kandidátek se prosadila pouze bývalá místopředsedkyně strany Pavla Brady v Moravskoslezském kraji; v dalších sedmi krajích jsou lídry muži, jinde zelení přepustili místo lídra koaličním stranám.

Nezvykle vysoký podíl žen – kolem 70 procent – mají strany sdružené v koalici Ne ilegální imigraci – peníze raději pro naše lidi.

„Ženy mají strach z hmoty mužů, která se sem hrne,“ vysvětluje anomálii Petr Hannig, předseda Strany zdravého rozumu, který protiimigrační koalici stvořil. „Mají také silnější pud sebezáchovy.“

Navzdory většině žen na kandidátkách jsou ale i u něj lídry převážně muži, na třináct kandidátek strana postavila pouze čtyři lídryně. Slabší zastoupení Hannig vysvětluje tím, že ženy rády podpoří jeho program, ale nerady se účastní stranických schůzí a nechtějí být příliš vidět.

Celkově je na kandidátkách napříč politickým spektrem třicet procent žen. Oproti minulým krajským volbám je to o tři procentní body více.

## Velké strany mají dvě třetiny kandidátů s titulem. Výjimkou jsou komunisti

Nejvzdělanější celorepublikovou stranou jsou podle krajských kandidátek *křesťanští demokraté*. Akademickým titulem se může pochlubit 78 procent jejich kandidátů. Mezi nimi jsou nejčastější inženýři, magistři a lékaři.

<aside class="big">
  <iframe src="https://samizdat.cz/data/volby-2016-kandidatky/charts/tituly.html" class="ig" width="1024" height="600" scrolling="no" frameborder="0"></iframe>
</aside>

Většina parlamentních stran se pohybuje kolem dvou třetin vysokoškolsky vzdělaných kandidátů. Výjimkou je *KSČM*, akademický titul má pouze 42 procent stranických kandidátů. Sedmnáct z nich se honosí titulem doktor sociálně-politických věd (RSDr.), udělovaným za minulého režimu absolventům stranických vysokých škol. Nižší počet titulů souvisí také s věkovou strukturou – s věkovým průměrem 52 let jsou komunisté nejstarší stranou.

Na chvostu statistiky se pohybují protisystémové subjekty: *obě republikánské strany*, *Dělnická strana sociální spravedlnosti* a *Česká strana národně sociální*.